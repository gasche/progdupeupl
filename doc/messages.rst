Private messages app
====================

.. automodule:: pdp.messages
    :members:

Models
------

.. automodule:: pdp.messages.models
    :members:

Forms
-----

.. automodule:: pdp.messages.forms
    :members:

Views
-----

.. automodule:: pdp.messages.views
    :members:
