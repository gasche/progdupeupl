Article app (deprecated)
========================

.. automodule:: pdp.article
    :members:

Models
------

.. automodule:: pdp.article.models
    :members:

Views
-----

.. automodule:: pdp.article.views
    :members:
